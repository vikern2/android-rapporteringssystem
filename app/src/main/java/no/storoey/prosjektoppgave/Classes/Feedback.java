package no.storoey.prosjektoppgave.Classes;


import java.io.Serializable;

/**
 * Created by Erik on 5/8/2018.
 */

public class Feedback implements Serializable{
    private final String feedback;
    private final String date;
    private final String username;

    public Feedback(String feedback, int id, String date, int submitid, int writtenBy, String username) {
        this.feedback = feedback;
        int id1 = id;
        this.date = date;
        int submitid1 = submitid;
        int writtenBy1 = writtenBy;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public String getFeedback() {
        return feedback;
    }

    public String getDate() {
        return date;
    }

}
