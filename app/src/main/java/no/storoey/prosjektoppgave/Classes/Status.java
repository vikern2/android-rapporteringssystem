package no.storoey.prosjektoppgave.Classes;

/**
 * Created by Fredrik
 */

public class Status {
    private final int id;
    private final String text;

    public Status(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

}
