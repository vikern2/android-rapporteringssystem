package no.storoey.prosjektoppgave.Classes;

/**
 * Created by Erik on 5/15/2018.
 */

public class Category {
    private final int id;
    private final String text;
    private int count = 0;

    public Category(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void incrementCount(){
        count++;
    }

    public int getCount() {
        return count;
    }

}
