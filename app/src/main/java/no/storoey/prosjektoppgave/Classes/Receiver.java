package no.storoey.prosjektoppgave.Classes;

/**
 * Created by Fredrik
 */

public class Receiver {
    private final int id;
    private final String name;
    private final boolean isGroup;

    public Receiver(int id, String name, boolean isGroup) {
        this.id = id;
        this.name = name;
        this.isGroup = isGroup;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isGroup() {
        return isGroup;
    }

}
