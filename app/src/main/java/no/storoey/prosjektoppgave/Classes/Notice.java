package no.storoey.prosjektoppgave.Classes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Erik on 5/8/2018.
 */

public class Notice implements Serializable{
    private final int id;
    private final int submitterid;
    private final String submittername;
    private String category;
    private String content;
    private String status;
    private final String time;
    private final boolean anonymous;
    private final ArrayList<Feedback> feedback;

    public Notice(int id, int submitterid, String submittername, String category, String content, String status, int target, String time, String submitschange, String anon, ArrayList<Feedback> feedback) {
        this.id = id;
        this.submitterid = submitterid;
        this.submittername = submittername;
        this.category = category;
        this.content = content;
        this.status = status;
        this.time = time;
        this.anonymous = anon.equals("1");
        this.feedback = feedback;
    }

    public int getId() {
        return id;
    }

    public int getSubmitterid() {
        return submitterid;
    }

    public String getSubmittername() {
        return (this.anonymous ? "Anon" : submittername);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusInt() {
        switch (this.status) {
            case "Sett":
                return 1;
            case "Under behandling":
                return 2;
            case "Ferdig behandlet":
                return 3;
            default:
                return 0;
        }
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public String getTime() {
        return time;
    }

    public ArrayList<Feedback> getFeedback() {
        return feedback;
    }

}
