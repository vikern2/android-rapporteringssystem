package no.storoey.prosjektoppgave.Classes;

/**
 * Created by Erik on 5/24/2018.
 */

public class Progress {
    private final String text;
    private int count;

    public Progress(int id, String text, int count) {
        int id1 = id;
        this.text = text;
        this.count = count;
    }

    public void incrementCount(){
        count++;
    }

    public String getText() {
        return text;
    }

    public int getCount() {
        return count;
    }

}
