package no.storoey.prosjektoppgave.Classes;

import com.android.volley.VolleyError;

/**
 * Created by Fredrik
 */

public class CustomError extends VolleyError {
    private final String message;

    public CustomError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
