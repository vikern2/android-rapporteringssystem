package no.storoey.prosjektoppgave.LikeEnums;

/**
 * All the urls used
 */
public class Urls {
    private static final String URL = "https://kark.uit.no/~fst034/api/public/";

    public static final String LOGIN = URL + "login";
    public static final String SILENT = URL + "silent";
    public static final String USERSPECIFIC = URL + "get/";
    public static final String ALLNOTICES = URL + "allVarsler";
    public static final String TARGETSPECIFIC = URL + "targetVarsler/";
    public static final String CATEGORIES = URL + "getCategories";
    public static final String GETSTATUSES = URL + "getAllStatuses";
    public static final String STATUSCHANGE = URL + "submitStatusChange";
    public static final String SUBMITFEEDBACK = URL + "submitComment";
    public static final String GETRECEIVERS = URL + "getReceivers";
    public static final String NEWNOTICE = URL + "newVarsel";
    public static final String PASSWORDCHANGE = URL + "changePassword";
    public static final String EDITNOTICE = URL + "editVarsel";
}
