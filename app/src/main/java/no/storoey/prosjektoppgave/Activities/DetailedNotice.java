package no.storoey.prosjektoppgave.Activities;

/*
  Created by Erik on 5/15/2018.
 */


import android.app.Activity;
import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import java.util.Objects;
import no.storoey.prosjektoppgave.Adapters.FeedbackListViewAdapter;
import no.storoey.prosjektoppgave.Classes.Notice;
import no.storoey.prosjektoppgave.LikeEnums.Codes;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;

public class DetailedNotice extends AppCompatActivity {
    private Controller controller;
    private int position;
    private boolean isMine;
    private Notice notice;

    private boolean isUpdated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_notice);

        try {
            Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        } catch (Exception ignored) {}

        controller = Controller.getInstance(getApplicationContext());

        Intent intent = getIntent();
        position = intent.getIntExtra("notice", -1);
        isMine = intent.getBooleanExtra("mine", true);

        if (position == -1) {
            Toast.makeText(getApplicationContext(), "Relaunch app, and try again", Toast.LENGTH_LONG).show();
            finish();
        }

        Log.d("DETAIL", String.valueOf(isMine));

        if (isMine) {
            notice = controller.myNotices.get(position);
        } else {
            notice = controller.allNotices.get(position);
        }

        loadInformation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (controller.user.getUsertypeid() > 1 && !String.valueOf(notice.getSubmitterid()).equals(controller.user.getUserid())) {
            getMenuInflater().inflate(R.menu.hr_details_menu, menu);
        } else {
            if (notice.getStatusInt() == 0) {
                getMenuInflater().inflate(R.menu.user_details_menu, menu);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.handleNoticeItem:
                startHandling();
                break;
            case R.id.editNoticeItem:
                startEditing();
                break;
            case android.R.id.home:
                Intent returnIntent = new Intent();
                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, returnIntent);
                } else {
                    getParent().setResult(Activity.RESULT_OK, returnIntent);
                }
                finish();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, returnIntent);
        } else {
            getParent().setResult(Activity.RESULT_OK, returnIntent);
        }
        finish();
    }

    /**
     * Fills the Views with information from the selected notice
     */
    private void loadInformation() {
        TextView categoryTv = findViewById(R.id.detailsCategory);
        TextView contentTv = findViewById(R.id.detailsContent);
        final TextView statusTv = findViewById(R.id.detailsStatus);
        TextView sender = findViewById(R.id.detailsSender);
        TextView date = findViewById(R.id.detailsDate);

        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dtSubmitted = formatter.parseDateTime(notice.getTime());
        String dateString = dtSubmitted.getDayOfMonth() + ". " + dtSubmitted.monthOfYear().getAsText();

        sender.setText(notice.getSubmittername());
        categoryTv.setText(notice.getCategory());
        contentTv.setText(notice.getContent());
        statusTv.setText(notice.getStatus());
        date.setText(dateString);

        FeedbackListViewAdapter adapter = new FeedbackListViewAdapter(this, notice.getFeedback());
        ListView listView = findViewById(R.id.detailsFeedback);
        listView.setAdapter(adapter);

        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        if (totalHeight > 200) {
            totalHeight = 200;
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();

        if(!String.valueOf(notice.getSubmitterid()).equals(controller.user.getUserid()) && controller.user.getUsertypeid() > 1 && Objects.equals(notice.getStatus(), "Ikke sett")){
            controller.updateStatus(notice.getId(), 1);
        }
    }

    /**
     * Starts the activity where you can handle the notice
     */
    private void startHandling() {
        Intent intent = new Intent(this, HandleNotice.class);
        intent.putExtra("mine", isMine);
        intent.putExtra("notice", position);
        startActivityForResult(intent, Codes.NOTICE_CHANED);
    }

    /**
     * Starts the activity where you can edit the notice
     */
    private void startEditing() {
        Intent intent = new Intent(this, NewNoticeActivity.class);
        intent.putExtra("mine", isMine);
        intent.putExtra("notice", position);
        intent.putExtra("editing", true);
        startActivityForResult(intent, Codes.NOTICE_CHANED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Codes.NOTICE_CHANED) {
            isUpdated = true;
            if (isMine) {
                notice.setStatus(controller.myNotices.get(position).getStatus());
            } else {
                notice.setStatus(controller.allNotices.get(position).getStatus());
            }
        }

        loadInformation();
    }
}
