package no.storoey.prosjektoppgave.Activities;

import android.app.Activity;
/*
  Created by Erik on 5/16/2018.
  Fredrik also contributed
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import no.storoey.prosjektoppgave.Classes.Feedback;
import no.storoey.prosjektoppgave.Classes.Notice;
import no.storoey.prosjektoppgave.Classes.Status;
import no.storoey.prosjektoppgave.Interfaces.VolleyCallback;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;

public class HandleNotice extends AppCompatActivity {
    private Controller controller;

    private int position;
    private Notice notice;
    private Status selectedStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handle_notice);

        controller = Controller.getInstance(getApplicationContext());

        try {
            Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        } catch (Exception ignored) {

        }

        Intent intent = getIntent();

        position = intent.getIntExtra("notice", -1);
        final boolean isMine = intent.getBooleanExtra("mine", true);

        if (position == -1) {
            Toast.makeText(getApplicationContext(), "Relaunch app, and try again", Toast.LENGTH_LONG).show();
            finish();
        }

        Log.d("HANDLE", String.valueOf(isMine));

        if (isMine) {
            notice = controller.myNotices.get(position);
        } else {
            notice = controller.allNotices.get(position);
        }

        final EditText feedbackEt = findViewById(R.id.feedback);

        final ArrayList<Status> statuses = controller.getLocalStatuses();

        Spinner spinner = findViewById(R.id.handleNoticeSpinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String item = adapterView.getItemAtPosition(position).toString();

                for (Status status: statuses) {
                    if (status.getText().equals(item)) {
                        selectedStatus = status;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        List<String> statusDropdownList = new ArrayList<>();

        int statusIndex = 0;
        for (Status status: statuses) {
            statusDropdownList.add(status.getText());

            if (!status.getText().equals(notice.getStatus())) {
                statusIndex = statuses.indexOf(status);
                selectedStatus = status;
            }
        }

        ArrayAdapter<String> statusAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, statusDropdownList);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setSelection(statusIndex);

        spinner.setAdapter(statusAdapter);

        final ImageButton sendButton = findViewById(R.id.button);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            final String feedback = feedbackEt.getText().toString();

            controller.submitFeedback(notice.getId(), controller.user.getUserid(), feedback, selectedStatus.getId(), new VolleyCallback() {
                /**
                 *
                 * @param result response from server.
                 *               If status = true, the feedback was added to the db, and feedback is added to the list.
                 */
                @Override
                public void onSuccess(JSONObject result) {
                    try {
                        if (result.getBoolean("status")) {

                            if (isMine) {
                                controller.myNotices.get(position).setStatus(selectedStatus.getText());
                            } else {
                                controller.allNotices.get(position).setStatus(selectedStatus.getText());
                            }
                            //controller.allNotices.get(position).setStatus(selectedStatus.getText());

                            if (feedback.length() != 0) {
                                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                                String stringBuilder = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + " " +
                                        calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
                                Feedback newestFeedback = new Feedback(feedback, -1, stringBuilder, notice.getId(), Integer.parseInt(controller.user.getUserid()), controller.user.getUsername());

                                if (isMine) {
                                    controller.myNotices.get(position).getFeedback().add(newestFeedback);
                                } else {
                                    controller.allNotices.get(position).getFeedback().add(newestFeedback);
                                }
                            }


                            Toast.makeText(getApplicationContext(), "Feedback succesfully submitted", Toast.LENGTH_LONG).show();

                            Intent returnIntent = new Intent();
                            if (getParent() == null) {
                                setResult(Activity.RESULT_OK, returnIntent);
                            } else {
                                getParent().setResult(Activity.RESULT_OK, returnIntent);
                            }
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), result.getString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            }
        });
    }
}
