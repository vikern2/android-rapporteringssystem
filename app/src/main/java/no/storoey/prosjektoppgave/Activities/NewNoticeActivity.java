package no.storoey.prosjektoppgave.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import no.storoey.prosjektoppgave.Classes.Category;
import no.storoey.prosjektoppgave.Classes.Notice;
import no.storoey.prosjektoppgave.Classes.Receiver;
import no.storoey.prosjektoppgave.Interfaces.VolleyCallback;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;

public class NewNoticeActivity extends AppCompatActivity {

    private Controller controller;

    private Category selectedCategory;
    private Receiver selectedReceiver;

    private EditText descEditText;
    private Switch gdprSwitch;
    private Switch anonSwitch;
    private Button sendButton;

    private Notice notice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notice);

        controller = Controller.getInstance(getApplicationContext());

        Intent intent = getIntent();
        final boolean shouldEdit = intent.getBooleanExtra("editing", false);
        int noticeIndex = intent.getIntExtra("notice", -1);
        if (shouldEdit) {
            if (noticeIndex == -1) {
                notice = null;
            } else {
                notice = controller.myNotices.get(noticeIndex);
            }
        }

        final ArrayList<Category> categories = controller.getLocalCategories();

        Spinner categoryDropdown = findViewById(R.id.newCategoryDropdown);

        categoryDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String item = adapterView.getItemAtPosition(position).toString();
                ArrayList<Category> categories = controller.getLocalCategories();

                for (Category category: categories) {
                    if (category.getText().equals(item)) {
                        selectedCategory = category;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        List<String> categoryDropdownList = new ArrayList<>();

        for (Category categori: categories) {
            categoryDropdownList.add(categori.getText());
        }

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categoryDropdownList);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categoryDropdown.setAdapter(categoryAdapter);

        ArrayList<Receiver> receivers = controller.getLocalReceivers();

        Spinner receiverDropdown = findViewById(R.id.newReceiverDropdown);
        receiverDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String item = adapterView.getItemAtPosition(position).toString();
                ArrayList<Receiver> receivers = controller.getLocalReceivers();

                for (Receiver receiver: receivers) {
                    if (receiver.getName().equals(item)) {
                        selectedReceiver = receiver;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        List<String> receiversDropdownList = new ArrayList<>();

        int position = 0;
        boolean hasAddedUserTitle = false;
        int userTitlePosition = -1;
        for (Receiver receiver: receivers) {
            if (position == 0) {
                receiversDropdownList.add("Gruppe");
                position++;
            } else if (!receiver.isGroup() && !hasAddedUserTitle) {
                receiversDropdownList.add("Bruker");
                userTitlePosition = position;
                hasAddedUserTitle = true;
                position++;
            }

            receiversDropdownList.add(receiver.getName());
            position++;
        }

        final int finalUserTitlePosition = userTitlePosition;
        ArrayAdapter<String> receiverAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, receiversDropdownList) {
            @Override
            public boolean isEnabled(int position) {
                return position != finalUserTitlePosition && position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == finalUserTitlePosition || position == 0) {
                    textView.setTextColor(Color.GRAY);
                } else {
                    textView.setTextColor(Color.BLACK);
                }

                return view;
            }
        };
        receiverAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        receiverDropdown.setAdapter(receiverAdapter);

        descEditText = findViewById(R.id.newDescription);
        gdprSwitch = findViewById(R.id.newGdprSwitch);
        anonSwitch = findViewById(R.id.newAnonSwitch);
        sendButton = findViewById(R.id.newSendBtn);

        final int finalNoticeIndex = noticeIndex;
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String description = descEditText.getText().toString();

                if (selectedReceiver == null) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.new_receiver) + " " + getResources().getString(R.string.is_required), Toast.LENGTH_LONG).show();
                    return;
                }

                if (description.length() == 0) {
                    descEditText.setError(getResources().getString(R.string.new_description) + " " + getResources().getString(R.string.is_required));
                    return;
                }

                if (!gdprSwitch.isChecked()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.new_gdpr_short), Toast.LENGTH_LONG).show();
                    return;
                }

                controller.hideKeyboard(getCurrentFocus());

                String receiver;
                if (selectedReceiver.isGroup()) {
                    receiver = selectedReceiver.getId() + "_" + selectedReceiver.getName();
                } else {
                    receiver = String.valueOf(selectedReceiver.getId());
                }

                if (shouldEdit) {
                    controller.editNotice(selectedCategory.getId(), receiver, description, notice.getId(), new VolleyCallback() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.edit_sent), Toast.LENGTH_LONG).show();
                            controller.myNotices.get(finalNoticeIndex).setContent(descEditText.getText().toString());
                            controller.myNotices.get(finalNoticeIndex).setCategory(selectedCategory.getText());
                            finish();
                        }

                        @Override
                        public void onError(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    controller.sendNewNotice(selectedCategory.getId(), receiver, description, anonSwitch.isChecked(), new VolleyCallback() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.new_sent), Toast.LENGTH_LONG).show();
                            finish();
                        }

                        @Override
                        public void onError(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        if (shouldEdit) {
            updateForEdit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * When you edit a notice, this will update som of the ui to responde to the required input
     */
    private void updateForEdit() {
        descEditText.setText(notice.getContent());
        anonSwitch.setVisibility(View.INVISIBLE);
        sendButton.setText(getResources().getString(R.string.btn_save));
    }
}
