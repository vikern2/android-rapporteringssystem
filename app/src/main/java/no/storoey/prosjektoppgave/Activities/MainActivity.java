package no.storoey.prosjektoppgave.Activities;

/*
  Created by Fredrik on 4/8/2018.
 */

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import no.storoey.prosjektoppgave.Classes.Category;
import no.storoey.prosjektoppgave.Classes.Receiver;
import no.storoey.prosjektoppgave.Classes.Status;
import no.storoey.prosjektoppgave.Fragments.AllNoticesFragment;
import no.storoey.prosjektoppgave.Fragments.HomeFragment;
import no.storoey.prosjektoppgave.Interfaces.VolleyCallback;
import no.storoey.prosjektoppgave.Fragments.MyNoticesFragment;
import no.storoey.prosjektoppgave.LikeEnums.Codes;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;
import no.storoey.prosjektoppgave.Fragments.StatisticsFragment;
import no.storoey.prosjektoppgave.Fragments.SettingsFragment;

public class MainActivity extends AppCompatActivity {
    private Controller controller;

    private DrawerLayout drawerLayout;

    private HomeFragment homeFragment;
    private MyNoticesFragment myNoticesFragment;
    private AllNoticesFragment allNoticesFragment;
    private StatisticsFragment statisticsFragment;
    private SettingsFragment settingsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.homeToolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24px);
        }

        controller = Controller.getInstance(getApplicationContext());

        drawerLayout = findViewById(R.id.homeDrawerLayout);
        NavigationView navigationView = findViewById(R.id.homeNavView);
        FloatingActionButton sendFab = findViewById(R.id.sendNewFab);

        homeFragment = new HomeFragment();
        myNoticesFragment = new MyNoticesFragment();
        settingsFragment = new SettingsFragment();

        if (controller.user.getUsertypeid() == 1) {
            navigationView.inflateMenu(R.menu.user_drawer_menu);
            controller.downloadMyNoticesAndSave(null);
        } else {
            navigationView.inflateMenu(R.menu.hr_drawer_menu);
            controller.downloadMyNoticesAndSave(null);
            controller.downloadAllAndSave(null);
            controller.downloadAllNoFilterAndSave(null);

            allNoticesFragment = new AllNoticesFragment();
            statisticsFragment = new StatisticsFragment();
        }

        changeContent(homeFragment);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                drawerLayout.closeDrawers();

                switch (item.getItemId()) {
                    case R.id.nav_user_home:
                    case R.id.nav_hr_home:
                        changeContent(homeFragment);
                        break;

                    case R.id.nav_user_my:
                    case R.id.nav_hr_my:
                        changeContent(myNoticesFragment);
                        break;

                    case R.id.nav_user_settings:
                    case R.id.nav_hr_settings:
                        changeContent(settingsFragment);
                        break;

                    case R.id.nav_user_signout:
                    case R.id.nav_hr_signout:
                        controller.clearStorage();
                        finish();
                        break;

                    case R.id.nav_hr_all:
                        changeContent(allNoticesFragment);
                        break;

                    case R.id.nav_hr_statistics:
                        changeContent(statisticsFragment);
                        break;
                }

                return true;
            }
        });

        sendFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewNoticeActivity.class);
                startActivityForResult(intent, Codes.NEW_NOTICE);
            }
        });

        controller.getCategories(new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    JSONArray cats = result.getJSONArray("data");

                    ArrayList<Category> receivedCategories = new ArrayList<>();

                    for (int i = 0; i < cats.length(); i++) {
                        JSONObject object = cats.getJSONObject(i);
                        receivedCategories.add(new Category(object.getInt("id"), object.getString("text")));
                    }

                    controller.saveLocalCategories(receivedCategories);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {

            }
        });

        controller.getReceivers(new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    JSONArray cats = result.getJSONArray("data");

                    ArrayList<Receiver> receivers = new ArrayList<>();

                    ArrayList<String> groupsAdded = new ArrayList<>();
                    for (int i = 0; i < cats.length(); i++) {
                        JSONObject object = cats.getJSONObject(i);

                        if (!groupsAdded.contains(object.getString("groupname"))) {
                            Receiver receiver = new Receiver(object.getInt("id"), object.getString("groupname"), true);

                            receivers.add(receiver);

                            groupsAdded.add(object.getString("groupname"));
                        }

                        if (!object.getString("userid").equals(controller.user.getUserid())) {
                            receivers.add(new Receiver(object.getInt("userid"), object.getString("name"), false));
                        }
                    }

                    controller.saveLocalReceivers(receivers);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {

            }
        });

        controller.getAllStatuses(new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    JSONArray cats = result.getJSONArray("data");

                    ArrayList<Status> receivedStatuses = new ArrayList<>();

                    for (int i = 0; i < cats.length(); i++) {
                        JSONObject object = cats.getJSONObject(i);
                        receivedStatuses.add(new Status(object.getInt("id"), object.getString("text")));
                    }

                    controller.saveLocalStatuses(receivedStatuses);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {

            }
        });
    }

    /**
     * Overriden to not goback to login if pressend
     */
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Changes the fragments/content
     * @param fragment the fragment to show
     */
    private void changeContent(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.homeFrameFragmentLayout, fragment);
        ft.commit();
    }
}
