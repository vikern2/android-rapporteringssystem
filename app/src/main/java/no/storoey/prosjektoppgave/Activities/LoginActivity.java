package no.storoey.prosjektoppgave.Activities;

/*
  Created by Fredrik on 4/8/2018.
 */

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import no.storoey.prosjektoppgave.Classes.User;
import no.storoey.prosjektoppgave.Interfaces.VolleyCallback;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;

public class LoginActivity extends AppCompatActivity {
    private Controller controller;

    private ProgressBar progressBar;
    private EditText usernameField;
    private EditText passwordField;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        controller = Controller.getInstance(getApplicationContext());

        String session = controller.getString("user_session");
        String userid = controller.getString("user_id");
        if (session != null && userid != null) {
            controller.silentLogin(session, userid, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject result) {
                    try {
                        if (!result.getBoolean("status")) {
                            Snackbar.make(findViewById(android.R.id.content), result.getString("message"), Snackbar.LENGTH_LONG).show();
                        } else {
                            JSONObject data = result.getJSONObject("data");

                            User user = new User();
                            user.setUserid(data.getString("userid"));
                            user.setUsername(data.getString("username"));
                            user.setSession(data.getString("session"));
                            user.setUsertypeid(Integer.parseInt(data.getString("usertypeid")));
                            user.setAgreementseen(data.getString("userid").equals("1"));
                            user.setEmail(data.getString("email"));

                            controller.user = user;
                            gotoHome();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(VolleyError error) {

                }
            });
        }

        progressBar = findViewById(R.id.loginProgressBar);
        usernameField = findViewById(R.id.loginUsername);
        passwordField = findViewById(R.id.loginPassword);
        loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitLogin();
            }
        });
    }

    /**
     * Simple method to validate field. Checks if text is not empty
     * @param field the field to check
     * @return boolean if field is empty or not
     */
    private boolean validateField(EditText field) {
        String value = field.getText().toString();

        return value.length() == 0;
    }

    /**
     * Method to submit the login from the activity
     */
    private void submitLogin() {
        controller.hideKeyboard(getCurrentFocus());

        if (validateField(usernameField)) {
            Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.username) + " " + getResources().getString(R.string.is_required), Snackbar.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), getResources().getString(R.string.username) + " " + getResources().getString(R.string.is_required), Toast.LENGTH_LONG).show();
            return;
        }

        if (validateField(passwordField)) {
            Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.password) + " " + getResources().getString(R.string.is_required), Snackbar.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), getResources().getString(R.string.password) + " " + getResources().getString(R.string.is_required), Toast.LENGTH_LONG).show();
            return;
        }

        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();

        toggleUI(false);
        controller.userLogin(username, password, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                toggleUI(true);

                try {
                    if (!result.getBoolean("status")) {
                        Snackbar.make(findViewById(android.R.id.content), result.getString("message"), Snackbar.LENGTH_LONG).show();
                        //Toast.makeText(getApplicationContext(), result.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        JSONObject data = result.getJSONObject("user");
                        User user = new User();
                        user.setUserid(data.getString("userid"));
                        user.setUsername(data.getString("username"));
                        user.setSession(data.getString("session"));
                        user.setUsertypeid(Integer.parseInt(data.getString("usertypeid")));
                        user.setAgreementseen(data.getString("userid").equals("1"));
                        user.setEmail(data.getString("email"));

                        controller.user = user;

                        controller.putString("user_session", user.getSession());
                        controller.putString("user_id", user.getUserid());

                        Log.d("LOGIN", data.toString());

                        passwordField.setText("");

                        Toast.makeText(getApplicationContext(), "Logged in", Toast.LENGTH_LONG).show();
                        gotoHome();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                toggleUI(true);

                Log.d("ERROR", error.toString());
                Snackbar.make(findViewById(android.R.id.content), error.getMessage(), Snackbar.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Disables/enables ui
     * @param toggleValue enable if true, disable if false
     */
    private void toggleUI(boolean toggleValue) {
        usernameField.setEnabled(toggleValue);
        passwordField.setEnabled(toggleValue);
        loginButton.setEnabled(toggleValue);
        progressBar.setVisibility((toggleValue ? View.INVISIBLE : View.VISIBLE));
    }

    /**
     * Goes to MainActivity if logged in or if logging in
     */
    private void gotoHome() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
