package no.storoey.prosjektoppgave.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import no.storoey.prosjektoppgave.Activities.DetailedNotice;
import no.storoey.prosjektoppgave.Adapters.ListViewAdapter;
import no.storoey.prosjektoppgave.Classes.Notice;
import no.storoey.prosjektoppgave.Interfaces.SimpleCallback;
import no.storoey.prosjektoppgave.LikeEnums.Codes;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;

/*
 * Originally created by Erik
 * Fredrik updated to Fragment
 */
public class AllNoticesFragment extends Fragment {
    private Controller controller;
    private ListViewAdapter adapter;
    private SwipeRefreshLayout swiper;
    private ProgressBar progressBar;
    private ListView listView;

    public AllNoticesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        controller = Controller.getInstance(getContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_notices, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swiper = view.findViewById(R.id.allNoticeRefresher);
        progressBar = view.findViewById(R.id.allNoticeProgressbar);
        listView = view.findViewById(R.id.allNotiesListView);

        fillListView(controller.allNotices);

        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                controller.downloadAllAndSave(new SimpleCallback() {
                    @Override
                    public void done() {
                        swiper.setRefreshing(false);
                        fillListView(controller.allNotices);
                        //adapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    /**
     * Fills the view with items and binds clicklistener
     * @param notices ArrayList of noticees to show in list
     */
    private void fillListView(ArrayList<Notice> notices) {
        progressBar.setVisibility(View.GONE);
        adapter = new ListViewAdapter(getContext(), notices);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetailedNotice(position);
            }
        });
    }

    /**
     * Opens intent to show the details
     * @param notice index of where the notice is in controller.myNotice/controller.allNotice
     */
    private void showDetailedNotice(int notice) {
        Intent intent = new Intent(getActivity(), DetailedNotice.class);
        intent.putExtra("mine", false);
        intent.putExtra("notice", notice);
        startActivity(intent);
    }

    /**
     * Overriden to update ui-items
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Codes.NOTICE_CHANED) {
            adapter.notifyDataSetChanged();
        }
    }
}
