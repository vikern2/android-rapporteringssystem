package no.storoey.prosjektoppgave.Fragments.StatisticsSubFragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import no.storoey.prosjektoppgave.Classes.Notice;
import no.storoey.prosjektoppgave.Classes.Progress;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;

/**
 * Created by Erik on 5/24/2018.
 */

public class StatisticsProgress extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.statistics_fragment, container, false);
        Controller controller = Controller.getInstance(getContext());

        ArrayList<Notice> notices = controller.allNoticesNoFilters;

        sortNoticesByProgress(notices);

        return rootView;
    }

    private void sortNoticesByProgress(ArrayList<Notice> notices) {
        Progress[] progresses = new Progress[4];
        progresses[0] = new Progress(0, "Ikke sett", 0);
        progresses[1] = new Progress(1, "Sett", 0);
        progresses[2] = new Progress(2, "Under behandling", 0);
        progresses[3] = new Progress(3, "Ferdig behandlet", 0);

        for (Notice notice: notices) {
            for (Progress progress: progresses){
                if(Objects.equals(progress.getText(), notice.getStatus())){
                    progress.incrementCount();
                }
            }
        }

        fillGraph(progresses);
    }

    private void fillGraph(Progress[] progresses) {
        PieChart chart = rootView.findViewById(R.id.chart);

        chart.setCenterText("Status");
        chart.setCenterTextSize(20f);
        chart.setHoleRadius(35f);
        chart.setTransparentCircleAlpha(40);
        chart.setTransparentCircleRadius(50f);
        chart.setTouchEnabled(false);

        chart.setDescription(null);

        Legend legend = chart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);

        List<PieEntry> entries = new ArrayList<>();

        for (Progress progress: progresses) {
            if (progress.getCount() != 0) {
                entries.add(new PieEntry(progress.getCount(), progress.getText()));
            }
        }

        PieDataSet dataSet = new PieDataSet(entries, "");

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(230, 126, 34));
        colors.add(Color.rgb(44, 62, 80));
        colors.add(Color.rgb(127, 140, 141));
        colors.add(Color.rgb(26, 188, 156));
        colors.add(Color.rgb(211, 84, 0));
        colors.add(Color.rgb(241, 196, 15));
        colors.add(Color.rgb(46, 204, 113));
        colors.add(Color.rgb(52, 152, 219));
        colors.add(Color.rgb(22, 160, 133));
        colors.add(Color.rgb(243, 156, 18));

        dataSet.setColors(colors);
        dataSet.setValueTextColor(Color.WHITE);

        PieData pieData = new PieData(dataSet);
        dataSet.setSliceSpace(2);
        dataSet.setValueTextSize(12);
        chart.setData(pieData);
        chart.invalidate();
    }

}
