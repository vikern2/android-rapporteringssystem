package no.storoey.prosjektoppgave.Fragments;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.Objects;

import no.storoey.prosjektoppgave.Interfaces.VolleyCallback;
import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Services.Controller;

/*
 * Originally created by Erik
 * Fredrik updated to Fragment
 */
public class SettingsFragment extends Fragment {
    private Controller controller;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        controller = Controller.getInstance(getContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Configuration configuration = getResources().getConfiguration();

        final EditText oldPw = view.findViewById(R.id.settingsOldPassword);
        final EditText newPw = view.findViewById(R.id.settingsNewPassword);
        final EditText newPwConf = view.findViewById(R.id.settingsConfirmPassword);

        final Button confirm = view.findViewById(R.id.settingsSavePassword);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPw.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.old_password) + " " + getResources().getString(R.string.is_required), Toast.LENGTH_LONG).show();
                    return;
                } else if (newPw.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.new_password) + " " + getResources().getString(R.string.is_required), Toast.LENGTH_LONG).show();
                    return;
                } else if (newPwConf.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.new_password_confirm) + " " + getResources().getString(R.string.is_required), Toast.LENGTH_LONG).show();
                    return;
                } else if (!newPw.getText().toString().equals(newPwConf.getText().toString())) {
                    Toast.makeText(getContext(), getResources().getString(R.string.password_not_same), Toast.LENGTH_LONG).show();
                    return;
                }

                confirm.setEnabled(false);
                controller.changePassword(controller.user.getUserid(), oldPw.getText().toString(), newPw.getText().toString(), newPwConf.getText().toString(), new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        confirm.setEnabled(true);

                        try {
                            if (result.getBoolean("status")) {

                                Toast.makeText(getContext(), getResources().getString(R.string.password_changed), Toast.LENGTH_LONG).show();
                            } else {
                                controller.hideKeyboard(getView());
                                Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()), result.getString("message"), Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        confirm.setEnabled(true);
                    }
                });
            }
        });

        ImageButton american = view.findViewById(R.id.enflag);
        american.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configuration.setLocale(new Locale("en"));
                getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                reload();
            }
        });

        ImageButton norwegian = view.findViewById(R.id.norwayflag);
        norwegian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configuration.setLocale(new Locale("no"));
                getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                reload();
            }
        });
    }

    private void reload(){
        NavigationView nav = Objects.requireNonNull(getActivity()).findViewById(R.id.homeNavView);
        nav.getMenu().getItem(0).setChecked(true);
        Objects.requireNonNull(getActivity()).recreate();
    }
}
