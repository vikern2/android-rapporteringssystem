package no.storoey.prosjektoppgave.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import no.storoey.prosjektoppgave.Fragments.StatisticsSubFragments.StatisticsCategory;
import no.storoey.prosjektoppgave.Fragments.StatisticsSubFragments.StatisticsProgress;
import no.storoey.prosjektoppgave.R;


/*
 * Originally created by Erik
 * Fredrik updated to Fragment
 */
public class StatisticsFragment extends Fragment {
    private GraphPagerAdapter adapter;

    public StatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new GraphPagerAdapter(getChildFragmentManager());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewPager vp = view.findViewById(R.id.statisticsViewPager);
        vp.setAdapter(adapter);
    }

    class GraphPagerAdapter extends FragmentPagerAdapter {
        GraphPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         *
         * @param position
         * @return returns the Fragment that relates to the position
         */
        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:
                    return new StatisticsCategory();
                case 1:
                    return new StatisticsProgress();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position){
            switch(position){
                case 0:
                    return "Category";
                case 1:
                    return "Progress";
                default:
                    return null;
            }
        }
    }
}
