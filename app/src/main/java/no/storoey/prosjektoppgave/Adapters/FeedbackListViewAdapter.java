package no.storoey.prosjektoppgave.Adapters;

/*
  Created by Erik on 5/8/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import java.util.ArrayList;

import no.storoey.prosjektoppgave.R;
import no.storoey.prosjektoppgave.Classes.Feedback;

public class FeedbackListViewAdapter extends BaseAdapter {

    private final ArrayList<Feedback> feedbackArrayList;
    private final LayoutInflater inflater;


    public FeedbackListViewAdapter(Context context, ArrayList<Feedback> feedbacks) {
        this.feedbackArrayList = feedbacks;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return feedbackArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return feedbackArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        Feedback feedback = (Feedback) getItem(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.feedback_lv_layout, viewGroup, false);
        }

        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dtSubmitted = formatter.parseDateTime(feedback.getDate());
        String dateString = dtSubmitted.getDayOfMonth() + ". " + dtSubmitted.monthOfYear().getAsText();

        TextView writtenByTv = convertView.findViewById(R.id.writtenby);
        TextView dateTv = convertView.findViewById(R.id.date);
        TextView feedbackTv = convertView.findViewById(R.id.feedback);

        dateTv.setText(dateString);
        feedbackTv.setText(feedback.getFeedback());
        writtenByTv.setText(String.valueOf(feedback.getUsername()));

        return convertView;
    }
}