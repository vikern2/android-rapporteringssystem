package no.storoey.prosjektoppgave.Adapters;

/*
  Created by Erik on 5/8/2018.
 */

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import java.util.ArrayList;

import no.storoey.prosjektoppgave.Classes.Notice;
import no.storoey.prosjektoppgave.R;


public class ListViewAdapter extends BaseAdapter {
    private final ArrayList<Notice> noticesList;
    private final LayoutInflater inflater;
    private final Context context;

    public ListViewAdapter(Context context, ArrayList<Notice> notices) {
        this.context = context;
        this.noticesList = notices;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return noticesList.size();
    }

    @Override
    public Object getItem(int position) {
        return noticesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        Notice notice = (Notice) getItem(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.notice_lv_layout, viewGroup, false);
        }

        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dtSubmitted = formatter.parseDateTime(notice.getTime());
        String dateString = dtSubmitted.getDayOfMonth() + "." + dtSubmitted.monthOfYear().getAsText();

        ConstraintLayout statusColor = convertView.findViewById(R.id.noticeItemStatusColor);
        TextView submitterTv = convertView.findViewById(R.id.noticeItemSubmitter);
        TextView dateTv = convertView.findViewById(R.id.noticeItemDate);
        TextView categoryTv = convertView.findViewById(R.id.noticeItemCategory);
        TextView contentTv = convertView.findViewById(R.id.noticeItemContent);

        switch (notice.getStatusInt()) {
            case 0:
                statusColor.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
                break;
            case 1:
                statusColor.setBackgroundColor(context.getResources().getColor(R.color.statusSeen));
                break;
            case 2:
                statusColor.setBackgroundColor(context.getResources().getColor(R.color.statusWork));
                break;
            case 3:
                statusColor.setBackgroundColor(context.getResources().getColor(R.color.statusDone));
                break;
        }
        categoryTv.setText(notice.getCategory());
        dateTv.setText(dateString);
        submitterTv.setText(notice.getSubmittername());
        contentTv.setText(notice.getContent());
        return convertView;
    }
}