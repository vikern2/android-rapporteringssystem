package no.storoey.prosjektoppgave.Interfaces;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by Fredrik
 * Callback for Volley async tasks
 */
public interface VolleyCallback {
    void onSuccess(JSONObject result);
    void onError(VolleyError error);
}
