package no.storoey.prosjektoppgave.Interfaces;

/**
 * Created by Fredrik
 * Just a callback from some async functions
 */
public interface SimpleCallback {
    void done();
}
