package no.storoey.prosjektoppgave.Services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import no.storoey.prosjektoppgave.Classes.Category;
import no.storoey.prosjektoppgave.Classes.CustomError;
import no.storoey.prosjektoppgave.Classes.Feedback;
import no.storoey.prosjektoppgave.Classes.Notice;
import no.storoey.prosjektoppgave.Classes.Receiver;
import no.storoey.prosjektoppgave.Classes.Status;
import no.storoey.prosjektoppgave.Classes.User;
import no.storoey.prosjektoppgave.Interfaces.SimpleCallback;
import no.storoey.prosjektoppgave.Interfaces.VolleyCallback;
import no.storoey.prosjektoppgave.LikeEnums.Urls;

/**
 * Created by Fredrik
 * Erik also contributed
 */
public class Controller {
    @SuppressLint("StaticFieldLeak")
    private static Controller ourInstance;

    private final Context context;
    private RequestQueue requestQueue;
    private final Storage storage;

    public User user;

    public ArrayList<Notice> myNotices;
    public ArrayList<Notice> allNotices;
    public ArrayList<Notice> allNoticesNoFilters;

    /**
     * Gets the same instance everywhere in the app
     * @param context is required for some of the functons
     * @return instance of class
     */
    public static Controller getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new Controller(context);
        }

        return ourInstance;
    }

    private Controller(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();
        this.storage = Storage.getInstance(context);

        this.myNotices = new ArrayList<>();
        this.allNotices = new ArrayList<>();
    }

    private RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(this.context);
        }

        return requestQueue;
    }

    /**
     * Hides the keyboard
     * @param view current view
     */
    public void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Connected to storageclass to save a string by ket
     * @param key reference
     * @param value value to save
     */
    public void putString(String key, String value) {
        storage.putString(key, value);
    }

    /**
     * Get saved string
     * @param key reference to get with
     * @return String from key
     */
    public String getString(String key) {
        return storage.getString(key);
    }

    /**
     * Clear storage,
     * removes everything!
     */
    public void clearStorage() {
        storage.clear();
    }

    /**
     * Gets locally saved categories
     * @return List of categories
     */
    public ArrayList<Category> getLocalCategories() {
        ArrayList<Category> returnList = new ArrayList<>();
        try {
            String jsonString = storage.getString("localCategories");

            if (jsonString == null) {
                return returnList;
            }

            JSONArray array = new JSONArray(jsonString);

            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);

                Category category = new Category(object.getInt("id"), object.getString("text"));

                returnList.add(category);
            }

            return returnList;
        } catch (JSONException e) {
            //e.printStackTrace();
            return returnList;
        }
    }

    /**
     * Saves an ArrayList of categories to localstorage
     * @param items ArrayList of categories
     */
    public void saveLocalCategories(ArrayList<Category> items) {
        JSONArray array = new JSONArray();

        try {
            for (Category item : items) {
                JSONObject object = new JSONObject();
                object.put("id", item.getId());
                object.put("text", item.getText());

                array.put(object);
            }

            storage.putString("localCategories", array.toString());
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    /**
     * Gets locally saved receivers
     * @return List of receivers
     */
    public ArrayList<Receiver> getLocalReceivers() {
        ArrayList<Receiver> returnList = new ArrayList<>();
        try {
            String jsonString = storage.getString("localReceivers");

            if (jsonString == null) {
                return returnList;
            }

            JSONArray array = new JSONArray(jsonString);

            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);

                Receiver receiver = new Receiver(object.getInt("id"), object.getString("name"), object.getBoolean("isGroup"));

                returnList.add(receiver);
            }

            return returnList;
        } catch (JSONException e) {
            //e.printStackTrace();
            return returnList;
        }
    }

    /**
     * Saves an ArrayList of receivers to localstorage
     * @param items ArrayList of receivers
     */
    public void saveLocalReceivers(ArrayList<Receiver> items) {
        JSONArray array = new JSONArray();

        try {
            for (Receiver item : items) {
                JSONObject object = new JSONObject();
                object.put("id", item.getId());
                object.put("name", item.getName());
                object.put("isGroup", item.isGroup());

                array.put(object);
            }

            storage.putString("localReceivers", array.toString());
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    /**
     * Gets locally saved statuses
     * @return List of statuses
     */
    public ArrayList<Status> getLocalStatuses() {
        ArrayList<Status> returnList = new ArrayList<>();
        try {
            String jsonString = storage.getString("localStatuses");

            if (jsonString == null) {
                return returnList;
            }

            JSONArray array = new JSONArray(jsonString);

            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);

                Status status = new Status(object.getInt("id"), object.getString("text"));

                returnList.add(status);
            }

            return returnList;
        } catch (JSONException e) {
            //e.printStackTrace();
            return returnList;
        }
    }

    /**
     * Saves an ArrayList of status to localstorage
     * @param items ArrayList of status
     */
    public void saveLocalStatuses(ArrayList<Status> items) {
        JSONArray array = new JSONArray();

        try {
            for (Status item : items) {
                JSONObject object = new JSONObject();
                object.put("id", item.getId());
                object.put("text", item.getText());

                array.put(object);
            }

            storage.putString("localStatuses", array.toString());
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    /**
     * A generic POST function to use with "all" post-request,
     * inserts special header required to communicate with server
     * @param callback get responses where needed
     * @param url the url to post to
     * @param json the data to post
     * @param tag volleytag
     */
    private void post(final VolleyCallback callback, String url, JSONObject json, String tag) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("session", user.getSession());

                return params;
            }
        };
        jsonObjectRequest.setTag(tag);
        requestQueue.add(jsonObjectRequest);
    }

    /**
     * A generic GET function to use with "all" get-request,
     * inserts special header required to communicate with server
     * @param callback get responses where needed
     * @param url the url to post to
     * @param tag volleytag
     */
    private void get(final VolleyCallback callback, String url, String tag) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("session", user.getSession());

                return params;
            }
        };
        jsonObjectRequest.setTag(tag);
        requestQueue.add(jsonObjectRequest);
    }

    /**
     * Method to log the user in
     * @param username username on server
     * @param password password connected to username
     * @param callback callback to get responce
     */
    public void userLogin(String username, String password, final VolleyCallback callback) {
        try {
            JSONObject data = new JSONObject();
            data.put("username", username);
            data.put("password", password);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Urls.LOGIN, data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onError(error);
                }
            });

            jsonObjectRequest.setTag("LOGIN");
            requestQueue.add(jsonObjectRequest);
        } catch (JSONException e) {
            callback.onError(new CustomError(e.getMessage()));
            //e.printStackTrace();
        }
    }

    /**
     * Method to log the user in in the background
     * @param session session you got when last logged in
     * @param userid userid of user with the session
     * @param callback callback to get responce
     */
    public void silentLogin(String session, String userid, final VolleyCallback callback) {
        try {
            JSONObject data = new JSONObject();
            data.put("session", session);
            data.put("userid", userid);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Urls.SILENT, data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    callback.onSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onError(error);
                }
            });

            jsonObjectRequest.setTag("LOGIN");
            requestQueue.add(jsonObjectRequest);
        } catch (JSONException e) {
            callback.onError(new CustomError(e.getMessage()));
            //e.printStackTrace();
        }
    }

    /**
     * Method to get notices from a user
     * @param userId userid to get notices from
     * @param callback callback to get responce
     */
    private void getUserSpecificNotices(String userId, final VolleyCallback callback) {
        get(callback, Urls.USERSPECIFIC + userId, "USERSPECIFICNOTICES");
    }

    /**
     * Method to get notices sent to specific user or group
     * @param groupTarget the group notices sent to, and select from
     * @param userId userid to get notices from
     * @param callback callback to get responce
     */
    private void getTargetSpecificVarsler(int groupTarget, String userId, final VolleyCallback callback) {
        get(callback, Urls.TARGETSPECIFIC + groupTarget + "/" + userId, "TARGETSPECIFICNOTICE");
    }

    /**
     * Method to get ALL notices
     * @param callback callback for response
     */
    private void getAllNotices(final VolleyCallback callback) {
        get(callback, Urls.ALLNOTICES, "GETALLNOTICES");
    }

    /**
     * Gets categories from server
     * @param callback callback to get response
     */
    public void getCategories(final VolleyCallback callback) {
        get(callback, Urls.CATEGORIES, "CATEGORIES");
    }

    /**
     * Gets all statuses from server
     * @param callback callback to get response
     */
    public void getAllStatuses(final VolleyCallback callback) {
        get(callback, Urls.GETSTATUSES, "GETSTATUS");
    }

    /**
     * Updates status for a specific notice
     * @param noticeId the id of notice to update
     * @param status the status to update
     */
    public void updateStatus(int noticeId, int status) {
        try {
            JSONObject data = new JSONObject();
            data.put("submitid", noticeId);
            data.put("statusid", status);

            post(new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject result) {

                }

                @Override
                public void onError(VolleyError error) {

                }
            }, Urls.STATUSCHANGE, data, "STATUSCHANGE");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send feedback on a specific notice
     * @param noticeId the notice to send feedback on
     * @param userid the userid of who sent it
     * @param feedback the feedbackcontent/comment to set
     * @param status a updated status of the notice
     * @param callback callback to get response
     */
    public void submitFeedback(int noticeId, String userid, String feedback, int status, final VolleyCallback callback) {
        try {
            JSONObject data = new JSONObject();
            data.put("data-id", noticeId);
            data.put("userid", userid);
            data.put("intern", "");
            data.put("kommentarer", feedback);
            data.put("newStatus", status);

            post(callback, Urls.SUBMITFEEDBACK, data, "SUBMITFEEDBACK");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets receivers from server
     * @param callback callback to get response
     */
    public void getReceivers(final VolleyCallback callback) {
        get(callback, Urls.GETRECEIVERS, "GETRECEIVERS");
    }

    /**
     * Send a new notice to the server
     * @param categoryId the category of the notice
     * @param receiverId the receiver of the notice
     * @param description the description/content of the notice
     * @param isAnon if the notice is anonymous
     * @param callback callback to get response
     */
    public void sendNewNotice(int categoryId, String receiverId, String description, boolean isAnon, final VolleyCallback callback) {
        JSONObject data = new JSONObject();
        try {
            data.put("userid", user.getUserid());
            data.put("cat", categoryId);
            data.put("receiver", receiverId);
            data.put("content", description);
            data.put("anon", isAnon);

            post(callback, Urls.NEWNOTICE, data, "NEWNOTICE");
        } catch (JSONException e) {
            callback.onError(new CustomError(e.getMessage()));
        }
    }

    /**
     * Update the notice
     * @param categoryId the new category
     * @param receiverId the new receiver
     * @param description the new description
     * @param noticeId the notice to update
     * @param callback callback to get response
     */
    public void editNotice(int categoryId, String receiverId, String description, int noticeId, final VolleyCallback callback) {
        JSONObject data = new JSONObject();
        try {
            data.put("content", description);
            data.put("cat", categoryId);
            data.put("receiver", receiverId);
            data.put("id", noticeId);

            post(callback, Urls.EDITNOTICE, data, "EDITNOTICE");
        } catch (JSONException e) {
            callback.onError(new CustomError(e.getMessage()));
        }
    }

    /**
     * Downloads all notices and saves them locally (specific for target)
     * @param callback callback with response
     */
    public void downloadAllAndSave(final SimpleCallback callback) {
        getTargetSpecificVarsler(user.getUsertypeid(), user.getUserid(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    JSONArray array = result.getJSONArray("data");
                    ArrayList<Notice> notices = new ArrayList<>();

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        JSONArray feedbackArray = object.getJSONArray("feedback");
                        ArrayList<Feedback> feedbacks = new ArrayList<>();

                        for (int j = 0; j < feedbackArray.length(); j++) {
                            JSONObject feedbackObj = feedbackArray.getJSONObject(j);
                            Feedback feedback = new Feedback(
                                    feedbackObj.getString("feedback"),
                                    feedbackObj.getInt("id"),
                                    feedbackObj.getString("date"),
                                    feedbackObj.getInt("submittid"),
                                    feedbackObj.getInt("writtenBy"),
                                    feedbackObj.getString("username")
                            );

                            feedbacks.add(feedback);
                        }

                        Notice notice = new Notice(
                                object.getInt("id"),
                                object.getInt("submitterid"),
                                object.getString("submittername"),
                                object.getString("category"),
                                object.getString("content"),
                                object.getString("status"),
                                object.getInt("target"),
                                object.getString("time"),
                                object.getString("submitschange"),
                                object.getString("anon"),
                                feedbacks
                        );

                        notices.add(notice);
                    }

                    allNotices = notices;
                    try {
                        callback.done();
                    } catch (Exception ignored) {}
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {

            }
        });
    }

    /**
     * Downloads all notices and saves them locally
     * @param callback callback with response
     */
    public void downloadAllNoFilterAndSave(final SimpleCallback callback) {
        getAllNotices(new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    JSONArray array = result.getJSONArray("data");
                    ArrayList<Notice> notices = new ArrayList<>();

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        JSONArray feedbackArray = object.getJSONArray("feedback");
                        ArrayList<Feedback> feedbacks = new ArrayList<>();

                        for (int j = 0; j < feedbackArray.length(); j++) {
                            JSONObject feedbackObj = feedbackArray.getJSONObject(j);
                            Feedback feedback = new Feedback(
                                    feedbackObj.getString("feedback"),
                                    feedbackObj.getInt("id"),
                                    feedbackObj.getString("date"),
                                    feedbackObj.getInt("submittid"),
                                    feedbackObj.getInt("writtenBy"),
                                    feedbackObj.getString("username")
                            );

                            feedbacks.add(feedback);
                        }

                        Notice notice = new Notice(
                                object.getInt("id"),
                                object.getInt("submitterid"),
                                object.getString("submittername"),
                                object.getString("category"),
                                object.getString("content"),
                                object.getString("status"),
                                object.getInt("target"),
                                object.getString("time"),
                                object.getString("submitschange"),
                                object.getString("anon"),
                                feedbacks
                        );

                        notices.add(notice);
                    }

                    allNoticesNoFilters = notices;
                    try {
                        callback.done();
                    } catch (Exception ignored) {}
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {

            }
        });
    }

    /**
     * Downloads all the noticesof currently logged user and saves them locally
     * @param callback callback for response
     */
    public void downloadMyNoticesAndSave(final SimpleCallback callback) {
        getUserSpecificNotices(user.getUserid(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    JSONArray array = result.getJSONArray("data");
                    ArrayList<Notice> notices = new ArrayList<>();

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        JSONArray feedbackArray = object.getJSONArray("feedback");
                        ArrayList<Feedback> feedbacks = new ArrayList<>();

                        for (int j = 0; j < feedbackArray.length(); j++) {
                            JSONObject feedbackObj = feedbackArray.getJSONObject(j);
                            Feedback feedback = new Feedback(
                                    feedbackObj.getString("feedback"),
                                    feedbackObj.getInt("id"),
                                    feedbackObj.getString("date"),
                                    feedbackObj.getInt("submittid"),
                                    feedbackObj.getInt("writtenBy"),
                                    feedbackObj.getString("username")
                            );

                            feedbacks.add(feedback);
                        }

                        Notice notice = new Notice(
                                object.getInt("id"),
                                object.getInt("submitterid"),
                                object.getString("submittername"),
                                object.getString("category"),
                                object.getString("content"),
                                object.getString("status"),
                                object.getInt("target"),
                                object.getString("time"),
                                object.getString("submitschange"),
                                object.getString("anon"),
                                feedbacks
                        );


                        notices.add(notice);
                    }

                    myNotices = notices;

                    try {
                        callback.done();
                    } catch (Exception ignored) {}
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {

            }
        });
    }

    /**
     * Changes the password of current user
     * @param userid userid of user to change password on
     * @param oldPassword the old password of the user
     * @param newPassword the new password of the user
     * @param newPasswordConfirm the new password of the user
     * @param callback callback for response
     */
    public void changePassword(String userid, String oldPassword, String newPassword, String newPasswordConfirm, final VolleyCallback callback) {
        try {
            JSONObject data = new JSONObject();
            data.put("userid", userid);
            data.put("old_password", oldPassword);
            data.put("new_password", newPassword);
            data.put("new_password_confirm", newPasswordConfirm);

            post(callback, Urls.PASSWORDCHANGE, data, "PASSWORDCHANGE");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

