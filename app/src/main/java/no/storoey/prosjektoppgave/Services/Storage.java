package no.storoey.prosjektoppgave.Services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Fredrik
 * A simple storageclass to help with storing data
 */
public class Storage {
    private static final String STORAGE_PREFS_NAME = "MeStorageToo";

    @SuppressLint("StaticFieldLeak")
    private static Storage ourInstance;

    private final SharedPreferences preferences;

    public static Storage getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new Storage(context);
        }

        return ourInstance;
    }

    private Storage(Context context) {
        preferences = context.getSharedPreferences(STORAGE_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(String key) {
        return preferences.getString(key, null);
    }

    public void clear() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}
